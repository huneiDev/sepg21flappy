﻿using UnityEngine;
using System.Collections;

public class GroundMover : MonoBehaviour {

    public float moveSpeed;

    [SerializeField]
    private FlappyMovement flappy;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        float flappyMag = flappy.GetComponent<Rigidbody2D>().velocity.x;
        transform.Translate(Vector2.right * flappyMag * Time.deltaTime);
	}
}
