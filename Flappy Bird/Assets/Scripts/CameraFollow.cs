﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

    [SerializeField]
    private float xOffset = 0.0f;

    private FlappyMovement flappyObject = null;

    void Start(){

        flappyObject = FindObjectOfType<FlappyMovement>();

    }

    private void Update(){
        transform.position = new Vector3(flappyObject.transform.position.x, transform.position.y,-10);
    }



}
