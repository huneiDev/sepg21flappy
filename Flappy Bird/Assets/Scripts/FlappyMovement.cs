﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class FlappyMovement : MonoBehaviour {

    [SerializeField]
    private float flappySpeed = 0.0f;

    [SerializeField]
    private Spawner spawner;
       
    [SerializeField]
    private bool canMove = true;

    [SerializeField]
    private float flapForce = 0.0f;
  
    public int flappyScore = 0;

    public int highScore = 0;

    private AudioSource audioSource;

    [SerializeField]
    private Text scoreText;

    [SerializeField]
    private GroundMover mover;

    [SerializeField]
    private GameObject endGameBoard;

    [SerializeField]
    private GameObject scoreTexts;

    private Rigidbody2D rb;

    [SerializeField]
    private List<Sprite> medals;

    [SerializeField]
    private SpriteRenderer medalPlaceholder;

    [SerializeField]
    private AudioClip flapClip;
   
    [SerializeField]
    private AudioClip dieClip;
   
    [SerializeField]
    private AudioClip pointClip;
    [SerializeField]
    private AudioClip hitClip;
   

    void Start(){

  
        Time.timeScale = 1;
        rb = GetComponent<Rigidbody2D>();
        audioSource = GetComponent<AudioSource>();
        Load();
    }

    public void PlaySound(string soundType){

        if (soundType.Equals("flap"))
        {
            audioSource.clip = flapClip;
        }
        else if (soundType.Equals("die"))
        {
            audioSource.clip = dieClip;

        }
        else if (soundType.Equals("hit"))
        {
            audioSource.clip = hitClip;
        }
        else if (soundType.Equals("point"))
        {
            audioSource.clip = pointClip;
        }

        audioSource.Play();

    }

    public void GoToMenu(){
       

        SceneManager.LoadScene("Menu");


    }

    private void ShowMedal(){

        if (flappyScore >= 1)
        {
            medalPlaceholder.sprite = medals[0];
            medalPlaceholder.gameObject.SetActive(true);
        }
        if (flappyScore >= 2)
        {
            medalPlaceholder.sprite = medals[1];
            medalPlaceholder.gameObject.SetActive(true);
        }

        if (flappyScore >= 3)
        {
            medalPlaceholder.sprite = medals[3];
            medalPlaceholder.gameObject.SetActive(true);
        }

        if (flappyScore >= 4)
        {
            medalPlaceholder.sprite = medals[2];
            medalPlaceholder.gameObject.SetActive(true);
        }


    }

    public void RestartGame(){

        SceneManager.LoadScene("Game");


    }

    private void Load(){

        if (PlayerPrefs.HasKey("flappyHighscore")) // Zaidimas buvo pajungtas jau antra karta
        {
            highScore = PlayerPrefs.GetInt("flappyHighscore");



        }
        else // Zaidimas buvo pajungtas pirma karta
        {
            highScore = 0;

        }


    }


    private void Save(){ // Jeigu mes pagerinom highscore, tada issaugom nauja skaiciu

        if (flappyScore > highScore)
        {

            highScore = flappyScore;
            PlayerPrefs.SetInt("flappyHighscore", flappyScore);


        }



    }

    private void OnCollisionEnter2D(Collision2D collision){ // Ivyksta kai atsirenkia flappy i kazka.

        if (collision.gameObject.tag == "Ground")
        {
            Save();
            endGameBoard.SetActive(true);
            scoreTexts.SetActive(true);
            Text scoreText = scoreTexts.transform.GetChild(0).GetComponent<Text>();
            Text highscoreText = scoreTexts.transform.GetChild(1).GetComponent<Text>();
            scoreText.text = flappyScore.ToString();
            highscoreText.text = highScore.ToString();
            mover.moveSpeed = 0;
            ShowMedal();
            Time.timeScale = 0;
            PlaySound("hit");

//            Application.LoadLevel("Game"); // Restartinu "Game" scena.
        }

    }

    private void UpdateScoreText(){

        scoreText.text = flappyScore.ToString();


    }

    private void OnTriggerEnter2D(Collider2D collision){

        if (collision.gameObject.tag == "Coin")
        {
            flappyScore++; // Prides prie dabartinio skaiciaus viena.
            UpdateScoreText();
            spawner.SpawnColone();
            PlaySound("point");
        }



    }

    private void Update(){

        rb.velocity = new Vector2(flappySpeed, rb.velocity.y);

       
        if (Input.GetMouseButtonDown(0))
        {
            rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y + flapForce);
            PlaySound("flap");

        }
            
    }


}
