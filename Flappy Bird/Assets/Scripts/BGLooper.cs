﻿using UnityEngine;
using System.Collections;

public class BGLooper : MonoBehaviour {


    [SerializeField]
    private int amountofBGS = 1;


    void OnTriggerEnter2D(Collider2D other){

        if (other.gameObject.tag == "BG")
        {
            float xOffset = other.gameObject.GetComponent<BoxCollider2D>().size.x;
            float x = (other.gameObject.transform.position.x + xOffset * amountofBGS) - 1;
            other.gameObject.transform.position = new Vector2(x, other.gameObject.transform.position.y);

        }


    }
}
