﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour {

    [SerializeField]
    FlappyMovement flappyMovement;

    [SerializeField]
    private float nextX;

    [SerializeField]
    private GameObject colone;

    [SerializeField]
    private float minY;

    [SerializeField]
    private float maxY;

    public void SpawnColone(){

        GameObject newColone = (GameObject)Instantiate(colone);
        float randomY = Random.Range(minY, maxY); // Sukurs betkoki skaiciu, skaiciu tieseje minY - maxY
        newColone.transform.position = new Vector2(nextX, randomY);
        nextX += 2;

    }

    void Start(){

        for (int i = 0; i < 5; i++) {

            SpawnColone();

        }


    }

	
}
